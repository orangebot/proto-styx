package Protocol::Styx;

use strict;
use warnings;
use utf8;


#Make a hash to hold the sizes of message parts that don't change.
#These amounts are grabbed from http://man.cat-v.org/9front/5/intro
#See also the Fcall struct https://github.com/9fans/plan9port/blob/master/include/fcall.h
my %SIZE = (
	afid	=>	qw[4 L],
	count	=>	qw[4 L],
	fid	=>	qw[4 L],
	iounit	=>	qw[4 L],
	mode	=>	qw[1 C],
	msize	=>	qw[4 L],
	newfid	=>	qw[4 L],
	nwname	=>	qw[2 S],
	nwqid	=>	qw[2 S],
	offset	=>	qw[8 B],
	oldtag	=>	qw[2 S],
	perm	=>	qw[4 L],
	qid_p	=>	qw[8 B],
	qid_t	=>	qw[1 C],
	qid_v	=>	qw[4 L],
	size	=>	qw[4 L],
	tag	=>	qw[2 S],
	type	=>	qw[1 C]
);


#Make a hash to hold the number assiciated with the type of message.
my %TYPE = (
	Tversion	=>	100,
	Rversion	=>	101,
	Tauth	=>	102,
	Rauth	=>	103,
	Tattach	=>	104,
	Rattach	=>	105,
	Terror	=>	106,
	Rerror	=>	107,
	Tflush	=>	108,
	Rflush	=>	109,
	Twalk	=>	110,
	Rwalk	=>	111,
	Topen	=>	112,
	Ropen	=>	113,
	Tcreate	=>	114,
	Rcreate	=>	115,
	Tread	=>	116,
	Rread	=>	117,
	Twrite	=>	118,
	Rwrite	=>	119,
	Tclunk	=>	120,
	Rclunk	=>	121,
	Tremove	=>	122,
	Rremove	=>	123,
	Tstat	=>	124,
	Rstat	=>	125,
	Twstat	=>	126,
	Rwstat	=>	127
);


#Make a sub to essentially emulate a `switch` statement.
sub switch {
	my $needle = shift;
	my %cases = @_;
	my $key = grep(&$needle, keys(%cases))[0];
	my $case = $cases{$key}
		|| $cases{else}
		|| sub { return undef };
	
	return &$case();
}


#Function to decode a byte and remove it from the buffer.
sub decode_chunk {
	#Get an defining the size. Either a key of %SIZE, or an array.
	my $type = $SIZE{$_[0]} || $_[0]; shift;
	#Get the buffer.
	my $buf = shift;
	
	#Get the chunk of data to parse.
	my $chunk = substr $$buf, 0, $size->[0];
	#Remove the chuk from the buffer.
	$$buf = substr $$buf, $size->[0];
	
	#Unpack the chunk and return it.
	return unpack($size->[1] . '<', $chunk);
}


#Function to decode a string from the buffer.
sub decode_string {
	#Get the buffer.
	my $buf = shift;
	
	#Get the size of the string.
	my $size = decode_chunk [2, S], $buf;
	#Return the UTF-8 string.
	return decode_chunk [$size, 'U0A' . $size], $buf;
}


#Function to decode a Qid. A Qid is defined as a
#type, version, and path.
sub decode_qid {
	#Get the buffer.
	my $buf = shift;
	my %qid;
	
	#Get the type.
	$qid{type} = decode_chunk 'qid_t', $buf;
	#Get the version.
	$qid{version} = decode_chunk 'qid_v', $buf;
	#Get the path.
	$qid{path} = decode_chunk 'qid_p', $buf;
	
	#Return the UTF-8 string.
	return %qid;
}


#Take in a message and turn it into a hash structure.
sub styx_decode {
	my $buf = shift;
	my %msg;
	
	#Small function to decode a chunk and save it into
	#the '%msg' hash.
	sub save_chunk {
		my $type = shift;
		$msg{$type} = decode_chunk $type, \$buf;
	}
	
	#Grab the basics.
	save_chunk($_) for (qw/size type tag/);
	
	is_type( sub { $msg{type} == $TYPE{$_} },
		Tversion	=>	sub {
			save_chunk 'msize';
			$msg{version} = decode_string \$buf;
		},
		Rversion	=>	sub {
			save_chunk 'msize';
			$msg{version} = decode_string \$buf;
		},
		Tauth	=>	sub {
			save_chunk 'afid';
			$msg{uname} = decode_string \$buf;
			$msg{aname} = decode_string \$buf;
		},
		Rauth	=>	sub {
			save_chunk 'afid';
			$msg{aqid} = decode_qid \$buf;
		},
		Tattach	=>	sub {
			save_chunk 'fid';
			save_chunk 'afid';
			$msg{uname} = decode_string \$buf;
			$msg{aname} = decode_string \$buf;
		},
		Rattach	=>	sub {
			$msg{qid} = decode_qid \$buf;
		},
		Terror	=>	sub {
			die 'Illegal message type Terror';
		},
		Rerror	=>	sub {
			$msg{ename} = decode_string \$buf;
		},
		Tflush	=>	sub {
			save_chunk 'oldtag';
		},
		Rflush	=>	sub {
			#Nothing else
			return 1;
		},
		Twalk	=>	sub {
			save_chunk($_) for qw/fid newfid nwname/;
			for (1..$msg{nwname}) {
				push $msg{wname}, decode_string(\$buf);
			}
		},
		Rwalk	=>	sub {
			save_chunk 'nwqid';
			for (1..$msg{nwqid}) {
				push $msg{wqid}, decode_qid(\$buf);
			}
		},
		Topen	=>	sub {
			save_chunk($_) for qw/fid mode/;
		},
		Ropen	=>	sub {
			$msg{qid} = decode_qid \$buf;
			save_chunk 'iounit';
		},
		Tcreate	=>	sub {
			save_chunk 'fid';
			$msg{name} = decode_string \$buf;
			save_chunk 'perm';
			save_chunk 'mode';
		},
		Rcreate	=>	sub {
			$msg{qid} = decode_qid \$buf;
			save_chunk 'iounit';
		},
		Tread	=>	sub {
			save_chunk($_) for qw/fid offset count/;
		},
		Rread	=>	sub {
			save_chunk 'count';
			$msg{data} = decode_chunk [$msg{count}, 'B' . $msg{count}], \$buf;
		},
		Twrite	=>	sub {
			save_chunk($_) for qw/fid offset count/;
			$msg{data} = decode_chunk [$msg{count}, 'B' . $msg{count}], \$buf;
		},
		Rwrite	=>	sub {
			save_chunk 'count';
		},
		Tclunk	=>	sub {
			save_chunk 'fid';
		},
		Rclunk	=>	sub {
			#No other data.
			return 1;
		},
		Tremove	=>	sub {
			save_chunk 'fid';
		},
		Rremove	=>	sub {
			#No other data.
			return 1;
		},
		Tstat	=>	sub {
			save_chunk 'fid';
		},
		Rstat	=>	sub {
			my $n = decode_chunk [2, S], \$buf;
			$msg{stat} = decode_chunk [$n, 'B' . $n], \$buf;
		},
		Twstat	=>	sub {
			save_chunk 'fid';
			my $n = decode_chunk [2, S], \$buf;
			$msg{stat} = decode_chunk [$n, 'B' . $n], \$buf;
		},
		Rwstat	=>	sub {
			#No other data.
			return 1;
		},
		else	=>	sub {
			#If this triggers, then something bad happened.
			die "Got bad type value: " . $msg{type} . "; could not decode";
		}
	);
	
	return %msg;
}


#Return the raw string of the next message.
sub styx_read {
	my $fh = shift or die 'No file handle passed to read_in';
	my $buf;
	
	read $fh, $buf, $SIZE{size};
	read $fh, $buf, unpack('L<', $buf) - 4, $SIZE{size};
	
	return $buf;
}


#Read the next message from the file handle.
sub next_msg {
	my $fh = shift or die 'No file handle passed to next_msg';
	my %msg;
	my $buf = styx_read $fh;
	
	my @dat = unpack '( LCv )<', $buf;
	$msg{size} = $dat[0];
	$msg{type} = $dat[1];
	$msg{tag} = $dat[1];
	
	return %msg;
}





#Magic.
42;
