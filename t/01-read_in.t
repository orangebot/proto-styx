#!/usr/bin/env perl

use Test::Most;
use utf8;

use lib 'lib';
use Protocol::Styx;


#This test checks to see if the `read_in` function is working properly.


my $BUF_DUP = 3;
my @STRINGS = (
	'a',
	'hello friend',
	'This is a longer string to test with, to make sure that everything is reading in properly',
	join('', ('a'..'z', 'A'..'Z', '0'..'9'))
);


sub check_read_in {
	my $str = shift
		or die 'No string passed to test with';
	
	#Pack the test string in a manner similar to how real data will.
	my $buf = pack '( LA* )<', (length($str) + 4), $str;
	
	#Duplicate the buffer a few times to make it more realistic;
	$buf = $buf x $BUF_DUP;
	
	#Open the string as a file so that the function can read from it.
	open my $fh, '<', \$buf;
	my ($len, $res) = unpack('( LA* )<', Protocol::Styx::styx_read($fh));
	close $fh;
	
	subtest 'Return value check' => sub {
		plan tests => 2;
		is $len, length($buf) / $BUF_DUP, 'Message length matches';
		is $res, $str, 'String returned matches';
	}
}


map { check_read_in $_ } @STRINGS;

done_testing ($#STRINGS + 1);
