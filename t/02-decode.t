#!/usr/bin/env perl

use Test::Most;
use utf8;

use lib 'lib';
use Protocol::Styx;


#Get files to test with.
my @files = <t/dat/ex/*.9p>;



#Read in the contents of a file.
sub slurp {
	my $f = shift;
	my $c = do {
		local $/ = undef;
		open my $fh, '<', $f
			or die "Can't open $f for reading";
		<$fh>;
	}
	return $c;
}


#Function to parse the description file.
sub pdesc {
	#Read in the file contents.
	my $desc = slurp shift;
	
	#Parse the description and return a list of matches.
	my @m = ($desc =~ m/(\([^)]+\)|\w+)\s*/g);
	shift @m; #Remove the arrow at the start.
	
	my %ret = (type => shift(@m), @m);
}



#This test checks the decoding function.

for my $file (@files) {
	subtest {
		#Read in the contents of the file, and save it to a buffer var.
		my $buf = slurp $file;
		
		#Attempt to decode.
		my %msg;
		lives_ok { %msg = Protocol::Styx::decode $buf; } "Decode lives";
		
		
	}
}


