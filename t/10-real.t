#!/usr/bin/env perl

use Test::Most;

use lib 'lib';
use Protocol::Styx;


#Grab the sample 9p files for parsing.
my @files = <t/dat/ex/*.9p>;


for my $file (@files) {
	note "Attempting to parse $file";
	
	open(my $fh, '<', $file)
		or die "Can't open $file: $!";
	
	lives_ok {
		while ( my %msg = Protocol::Styx::next_msg($fh) ) {
			
		}
	} "Parsing $file";
	
	close $fh;
}


done_testing($#files + 1);
