# Protocol::Styx

This module is designed to read and handle [Styx/9p2000][1] messages.

## Reference

A collection of resources regarding this protocol can be found at the [awesome-9p repository][2]. Items of particular note are:

- [9front's intro to 9p][3]
- [The Styx Architecture for Distributed Systems][4]



[1]: https://en.wikipedia.org/wiki/9P_(protocol)
[2]: https://github.com/henesy/awesome-9p
[3]: http://man.cat-v.org/9front/5/intro
[4]: http://doc.cat-v.org/inferno/4th_edition/styx

